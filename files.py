import os
import shutil
from pathlib import Path

# DEFINIR CAMINHO BASE
BASE_DIR = os.path.dirname(__file__)
BASE_CAN = os.path.join(BASE_DIR, "Canhotos")
BASE_PROC = os.path.join(BASE_DIR, "PROC")

# CRIAR UMA PASTA CANHOTOS
def canhotos():
    try:
        if os.path.exists(os.path.join(BASE_DIR, "Canhotos")) == False:
            os.mkdir(os.path.join(BASE_DIR, 'Canhotos'))
        else:
            print("Já existe a pasta!!")

    except KeyError:
        print(KeyError)


if not os.path.exists(BASE_CAN):
    os.makedirs(BASE_CAN)

for subdir, dirs, files in os.walk(BASE_PROC):
    for file in files:
        if len(file) == 16:
                
            if file.endswith('.jpg'):
                file_path = os.path.join(subdir, file)
                shutil.copy2(file_path, BASE_CAN)
                
            